<?php
 ob_start();
 session_start();
 error_reporting(0);
 include_once("../config.php");
 $hal = basename($_SERVER["PHP_SELF"]);
 //echo $hal;
 if( !isset( $_SESSION["username"] ) ){
    
    header("Location: login.php");
    
 }else{
    
 
?>
<html>
<head>
	<link rel="stylesheet" href="../css/h1.css" />
    <title><?php echo $pageTitle?></title>
    <link rel="stylesheet" href="../assets/fonts/iconic fill/iconic_fill.css"/>
    <link href="js/jquery-ui/css/smoothness/jquery-ui-1.10.3.custom.css" rel="stylesheet">
    <link href="js/jquery-ui/css/smoothness/jquery.ui.timepicker.css" rel="stylesheet">
    <script src="js/jquery-ui/js/jquery-1.9.1.js"></script>
    <script src="js/jquery-ui/js/jquery-ui-1.10.3.custom.js"></script>
    <script src="js/jquery-ui/js/jquery.ui.timepicker.js"></script>
    
    <script>
      
       
       $('#mainmenu ul li ul').hide().removeClass('sub-menu');
       $('#mainmenu ul li').hover(
            function () {
            $('ul', this).stop().slideDown(100);
        },
        function () {
        $('ul', this).stop().slideUp(100);
        });

    </script>

</head>
<body>
<div class="header">
	<h2 class="title"><br>Perpustakaan</h2>
</div>
<br>

<div class="Hor">
	<div class="Horizontal1">
		<ul>
			<li><a href="buku.php">Buku</a></li>
		
		</ul>
	</div>
	<div class="Horizontal2">
		<ul>
			<li><a href="user.php">User</a></li>
			<li><a href="data-peminjaman.php">Data Peminjaman</a></li>
		
		</ul>
	</div>
</div>

</body>
</html>

<?php
}
?>