<?php
session_start();
ob_start();
include_once( "../config.php" );

global $pdo;

if( isset( $_POST["login"] ) ){
    
    $username = $_POST["username"];
    $password = $_POST["password"];
    
    $query = $pdo->prepare( "select * from admin where username = :username and password = :password" );
    $query->bindValue( ":username", $username, PDO::PARAM_STR );
    $query->bindValue( ":password", $password, PDO::PARAM_STR );
    $query->execute();
    $arrLogin = $query->fetchAll();
    if( !empty( $arrLogin ) ){
        
        $_SESSION["username"] = $username;
        header("Location: index.php");
        
    }
    
}

?>
<!DOCTYPE html>
<html>
<head>
	<title>Selamat di Perpustakaan</title>
	<link rel="stylesheet" type="text/css" href="../css/index.css">
</head>
<body>
	<br/>
  <br/>	
	<br/>
	<div class="login">
	<br/>
		<form action="login.php" method="post">
			<div>
				<input type="text" name="username" id="username" placeholder="Username"/>
      </div>
			<div>
				<input type="password" name="password" id="password" placeholder="Password" />
</div>
<br>		
			<div>
				<input type="submit" name="login" value="Login" class="tombol">
</div>
     <center> <p class="message">Belum Punya Akun? <a href="header.php">Buat Akun Baru</a></p></center>
		</form>
	</div>
</body>

<script type="text/javascript">
	function validasi() {
		var username = document.getElementById("username").value;
		var password = document.getElementById("password").value;		
		if (username != "" && password!="") {
			return true;
		}else{
			alert('Username atau Password yang Anda masukkan salah !');
			return false;
		}
	}

</script>
</html>