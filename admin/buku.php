<?php

include_once("header.php");
include_once("../class/class.buku.php");


if( isset( $_GET["act"] ) ){
    
    $act = $_GET["act"];    
    
}else{
    
    $act = "list";
}


if( $act == "insert" ){
  
   
    
    if( !isset( $_POST["submit"] ) ){ //show form
        $dsnObj = new Buku();
        setPagename("Insert Data Buku");
        $dsnObj->showInputForm( "buku.php?act=insert" );    
    
        
    }else{ // handle posted data
        
        $postData["kodeBuku"] = $_POST["kodeBuku"];
        $postData["judulBuku"] = $_POST["judulBuku"];
        $postData["penulisBuku"] = $_POST["penulisBuku"];
        $postData["rakBuku"] = $_POST["rakBuku"];
        $postData["penerbitBuku"] = $_POST["penerbitBuku"];
        $postData["jmlhalamanBuku"] = $_POST["jmlhalamanBuku"];
        $postData["tahunterbitBuku"] = $_POST["tahunterbitBuku"];
        
        //var_export($postData);
        
        $dsnObj = new Buku( $postData );
                    
        $dsnObj->insert();
             
        setPagename("Data Berhasil disimpan!");
        closePageName();
        
        
        
        
    } 
       
    closePageName();
   
    
}elseif( $act == "edit" ){
    
    if( isset( $_GET["kodeBuku"] ) ){
        
        if( !isset( $_POST["submit"] ) ){
            $data["kodeBuku"] = $_GET["kodeBuku"];
            $dsnObj = new Buku($data);
            $formValues = $dsnObj->fetchByKodeBuku;
            
            
         //   $tglExplode = explode("-", $formValues[0]["tglLahirDosen"]);
            
           // $formValues[0]["tglLahirDosen"] = $tglExplode[1]."/".$tglExplode[2]."/".$tglExplode[0];
            
            setPagename("Edit Data Buku");
            $dsnObj->showEditForm( $formValues[0] );
            closePageName();
        }else{
            
            $postData["kodeBuku"] = $_POST["kodeBuku"];
            $postData["judulBuku"] = $_POST["judulBuku"];
            $postData["penulisBuku"] = $_POST["penulisBuku"];
            $postData["rakBuku"] = $_POST["rakBuku"];
            $postData["penerbitBuku"] = $_POST["penerbitBuku"];
            $postData["jmlhalamanBuku"] = $_POST["jmlhalamanBuku"];
            $postData["tahunterbitBuku"] = $_POST["tahunterbitBuku"];
            
            var_export( $postData );
          
            $dsnObj = new Buku( $postData );
            $dsnObj->update();
            setPagename("Data Berhasil disimpan!");
            closePageName();
            //header("Location: dosen.php");
        }
        
    }else{
        
        
        
    }
    
}elseif( $act == "delete" ){
    
    if( isset( $_GET["kodeBuku"] ) ){
        
        $kodeBuku = $_GET["kodeBuku"];
        $dsnObj = new Buku();
        $dsnObj->delete( $kodeBuku );
        header("Location: buku.php");
        
    }
    
}else{// list
    
    include_once("class.paging.php");
    
    $dsnObj = new Buku();
    
    $sqlstr = "select * from buku ";
    
    $page = 1; // no halaman
    $numShow = 50; // jumlah data yang ditampilkan
    if( isset( $_GET["hal"] ) ){
        
        $page = $_GET["hal"];
        
    }
    
    $startat = ( $page - 1 ) * $numShow;
    
    if( isset( $_POST["filter"] ) ){
            
            $_SESSION["filterBy"] = $_POST["filterBy"];
            $_SESSION["filterWord"] = $_POST["filterWord"];
            
    }
    
    if( isset( $_SESSION["filterBy"] ) && $_SESSION["filterBy"] != "all" ){
        
        $filterBy = $_SESSION["filterBy"];
        $filterWord = $_SESSION["filterWord"];
        
                
        if( $filterBy == "kode" ){
            
            $field = "kodeBuku";
            
        }elseif( $filterBy == "judul" ){
            
            $field = "judulBuku";
            
        }elseif( $filterBy == "penulis" ){
            
            $field = "penulisBuku";
            
        }elseif( $filterBy == "penerbit" ){
            
            $field = "penerbitBuku";
            
            
        }
                                
                                
        $sqlstr .= "where $field like '%$filterWord%' ";
        
    }
    
    $sqlstr .= "limit $startat, $numShow";
    
    $arrBuku = $dsnObj->fetchSome( $sqlstr );
    
    setPagename(" Data Buku");
    
    
    ?>
    <table class="tblview">
        <tr>
            <form method="post" action="buku.php">
            <td>Filter</td>
            <td>
                <select name="filterBy">
                    <option value="all" <?php echo $filterBy == "all" ? "selected" : "";?>>All</option>
                    <option value="kodeBuku" <?php echo $filterBy == "kodeBuku" ? "selected" : "";?>>Kode Buku</option>
                    <option value="judul" <?php echo $filterBy == "judul" ? "selected" : "";?>>Judul</option>
                    <option value="penulis" <?php echo $filterBy == "penulis" ? "selected" : "";?>>Penulis</option>
                    <option value="penerbit" <?php echo $filterBy == "penerbit" ? "selected" : "";?>>Penerbit</option>
                </select>
            </td>
            <td><input type="text" name="filterWord" value="<?php echo isset( $filterWord ) ? $filterWord : ""; ?>"/></td>
            <td><input type="submit" name="filter" value="Filter"/></td>
            
            </form>
        </tr>
        <tr>
            <th>No</th><th>Kode Buku</th><th>Judul Buku</th><th>Penulis</th><th>No Rak</th><th>Penerbit</th><th>Jumlah Halaman</th><th>Tahun Terbit</th><th><a href="buku.php?act=insert" class="iconic plus"></a></th>
        </tr>
        <?php
          $no =  0;
          foreach( $arrBuku as $buku ){
            $no++;
            $zebra = $no%2 == 0 ? $zebra=" class=\"zebra\" " : "";
            echo "<tr$zebra>";
            echo "<td>".$no."</td>";
            echo "<td>".$buku["kodeBuku"]."</td>";
            echo "<td>".$buku["judulBuku"]."</td>";
            echo "<td>".$buku["penulisBuku"]."</td>";
            echo "<td>".$buku["rakBuku"]."</td>";
            echo "<td>".$buku["penerbitBuku"]."</td>";
            echo "<td>".$buku["jmlhalamanBuku"]."</td>";
            echo "<td>".$buku["tahunterbitBuku"]."</td>";
            echo "<td><small><a href=\"buku.php?act=edit&kodeBuku=".$buku["kodeBuku"]."\" class=\"iconic pen\" title=\"Edit\"></a></small>&nbsp;&nbsp;<small><a href=\"buku.php?act=delete&kodeBuku=".$buku["kodeBuku"]."\" class=\"iconic x\" title=\"Delete\"></a></small>"; 
            
            echo "</tr>";
            
          }
        
        ?>
    </table>
    <div class="pageNav">
    <?php
    $pageObj = new Paging();
    
    $jumhal = $pageObj->jumlahHalaman( $no, $numShow );
    echo $pageObj->navHalaman( $page, $jumhal );
    
    ?>
    </div>
    <?php
    
    closePageName();
}

?>


<?php


include_once("footer.php");
?>