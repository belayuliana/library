<?php

class Buku {
    
    private $kode;
    private $judul;
    private $penulis;
    private $rak;
    private $penerbit;
    private $jmlhalaman;
    private $tahunterbit;
    
     public function __construct( $data = array() ){
               
        $this->kode = isset( $data["kodeBuku"] ) ? $data["kodeBuku"] : "";
        $this->judul = isset( $data["judulBuku"] ) ? $data["judulBuku"] : "";
        $this->penulis = isset( $data["penulisBuku"] ) ? $data["penulisBuku"]: "";
        $this->rak = isset( $data["rakBuku"] ) ? $data["rakBuku"] : "";
        $this->penerbit = isset( $data["penerbitBuku"] ) ? $data["penerbitBuku"] : "";
        $this->jmlhalaman = isset( $data["jmlhalamanBuku"] ) ? $data["jmlhalamanBuku"] : "";
        $this->tahunterbit =  isset( $data["tahunterbitBuku"] ) ? $data["tahunterbitBuku"] : "";
    }
    
    public function fetchAll(){
        
        global  $pdo;
            
        $query = $pdo->prepare("select * from buku order by judulBuku");
        $query->execute();
        return $query->fetchAll(); 
              
    }
    
    public function fetchSome( $sqltr ){
        
        global $pdo;
        
        $query = $pdo->prepare( $sqltr );
        $query->execute();
        //$query->debugDumpParams();
        //echo var_export( $query->errorInfo() );
        return $query->fetchAll();

    }
   
    public function fetchByKodeBuku(){
       
       global  $pdo;
       
       $query = $pdo->prepare("select * from buku where kodeBuku = :kodeBuku");
       $query->bindValue( ":kodeBuku", $this->kode, PDO::PARAM_STR );
       $query->execute();
       //echo var_export($query->errorInfo());
       return $query->fetchAll();
    
    }
   
   
    public function insert(){
      
    
      $sqlstr = "insert into buku values(:kodeBuku, :judulBuku, :penulisBuku, :rakBuku, :penerbitBuku, :jmlhalamanBuku, :tahunterbitBuku )";
      $this->bindQuery( $sqlstr );
    
    
    }
   
    public function update(){
    
      $sqlstr = " judulBuku = :judulBuku, penulisBuku = :penulisBuku, rakBuku = :rakBuku, penerbitBuku = :penerbitBuku, jmlhalamanBuku = :jmlhalamanBuku, tahunterbitBuku = :tahunterbitBuku "; 
      $this->bindQuery( $sqlstr );
    
    }
   
    public function delete( $kodeBuku ){
        global  $pdo;
        $query = $pdo->prepare( "delete from buku where kodeBuku = $kodeBuku" );
        $query->bindValue( ':kodeBuku', $kodeBuku, PDO::PARAM_STR );
        $query->execute();
        /*if($query->errorCode() == 0){
      
       }else{
        $query->debugDumpParams();
        $err = $query->errorInfo();
        echo "Error : ".$err[0]." ".$err[1]." ".$err[2] ;
       }*/
        
    
    }
    
    private function bindQuery( $sqlstr ){
      global  $pdo;
     
      $query = $pdo->prepare( $sqlstr );
      $query->bindValue( ':kodeBuku', $this->kode, PDO::PARAM_STR );
      $query->bindValue( ':judulBuku', $this->judul, PDO::PARAM_STR );
      $query->bindValue( ':penulisBuku', $this->penulis, PDO::PARAM_STR );
      $query->bindValue( ':rakBuku', $this->rak, PDO::PARAM_STR );
      $query->bindValue( ':penerbitBuku', $this->penerbit, PDO::PARAM_STR );
      $query->bindValue( ':jmlhalamanBuku', $this->jmlhalaman, PDO::PARAM_STR );
      $query->bindValue( ':tahunterbitBuku', $this->tahunterbit, PDO::PARAM_STR );
      
      //var_export($this);
      
      $query->execute();
    
      if($query->errorCode() == 0){
      
      }else{
        $query->debugDumpParams();
        $err = $query->errorInfo();
        echo "Error : ".$err[0]." ".$err[1]." ".$err[2] ;
      }
      
    }
    
    function showInputForm( $url ){
        global  $pdo;
        ?>
   
        <form method="post" action="<?php echo $url; ?>" enctype="multipart/form-data">
            
            <table>
                <tr>
                    <td>Kode Buku</td>
                    <td><input type="text" name="kodeBuku" required/></td>
                </tr>
                <tr>
                    <td>Judul Buku</td>
                    <td><input type="text" name="judulBuku"/></td>
                </tr>
                <tr>
                    <td>Penulis</td>
                    <td><input type="text" name="penulisBuku"/></td>
                </tr>
                <tr>
                    <td>No Rak</td>
                    <td><input type="text" name="rakBuku"/></td>
                </tr>
                <tr>
                    <td>Penerbit</td>
                    <td><input type="text" name="penerbitBuku"/></td>
                </tr>
                <tr>
                    <td>Jumlah Halaman</td>
                    <td><input type="text" name="jmlhalamanBuku"/></td>
                </tr>
                <tr>
                    <td>Tahun Terbit</td>
                    <td><input type="text" name="tahunterbitBuku"/></td>
                </tr>
                <tr>
                    <td><input type="submit" name="submit" value="Submit"/></td>
                </tr>
            </table>
        
        </form>
    
        
        <?php
        
    }
    
    function showEditForm( $arrData = array()){
        
        //var_export($arrData);
        ?>
        <form method="post" enctype="multipart/form-data">
            <table>
                <tr>
                    <td>Kode Buku</td>
                    <td><input type="text" name="kodeBuku" value="<?php echo $arrData["kodeBuku"]; ?>" readonly/></td>
                </tr>
                <tr>
                    <td>Judul Buku</td>
                    <td><input type="text" name="judulBuku" value="<?php echo $arrData["judulBuku"]; ?>"/></td>
                </tr>
                <tr>
                    <td>Penulis</td>
                    <td><input type="text" name="penulisBuku" value="<?php echo $arrData["penulisBuku"]; ?>"/></td>
                </tr>
                <tr>
                    <td>No Rak</td>
                    <td><input type="text" name="rakBuku" value="<?php echo $arrData["rakBuku"]; ?>"/></td>
                </tr>
                <tr>
                    <td>Penerbit</td>
                    <td><input type="text" name="penerbitBuku" value="<?php echo $arrData["penerbitBuku"]; ?>" ></td>
                </tr>
                <tr>
                    <td>Jumlah Halaman</td>
                    <td><input type="text" name="jmlhalamanBuku" value="<?php echo $arrData["jmlhalamanBuku"]; ?>" ></td>
                </tr>
                <tr>
                    <td>Tahun Terbit</td>
                    <td><input type="text" name="tahunterbitBuku" value="<?php echo $arrData["jmlhalamanBuku"]; ?>" /></td>
                </tr>
                <tr>
                    <td><input type="submit" name="submit" value="Update"/></td>
                </tr>
            </table>
            
        </form>
        
        <?php
        
    }

}

?>