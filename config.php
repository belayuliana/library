<?php

global $pdo;
$dbhost = 'localhost';
$dbusername = 'root';
$dbpassword = '';
$dbname = 'library';

try{
    
    $pdo = new PDO("mysql:host=$dbhost;dbname=$dbname", $dbusername, $dbpassword);

} catch( PDOException $e ){
    
    exit('Database error : '.$e);
    
}

function setPagename( $pagename ){
    
    echo "<div class=\"pagename\">
                <h4>$pagename</h4>
            </div>
            <div class=\"subcontent\">";
    
}

function closePageName(){
    
     echo "</div>
        </div>";
}

?>